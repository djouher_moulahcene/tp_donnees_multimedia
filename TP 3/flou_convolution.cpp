// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

float convolution(OCTET* Img,int x,int y, float masque[250],int nW, int nH ){  
	int somme = 0;
	int diviseur = 0;
	for(int dx=-M/2;dx<=M/2;dx++){  
		for(int dy=-M/2;dy<=M/2;dy++){
			if((x+dx) >= 0 && (x+dx) < nW && (y+dy) >= 0 && (y+dy) < nH): 
				somme += Img[(x + dx)*nW+(y + dy)]; 
				diviseur += masque[(dx + M/2)*nW+(dy + M/2)];
        } 
    }
	return somme/diviseur;
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;
  float masque[250]; 
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[3],"%d",&S);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);


 for (int i=0; i < nH; i++)
   for (int j=1; j < nW; j++)
     {
       ImgOut[i*nW+j] = convolution(ImgIn, i, j, masque,nW,nH);
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}