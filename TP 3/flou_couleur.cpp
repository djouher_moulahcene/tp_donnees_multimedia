#include "image_ppm.h"

void separateRGB(OCTET* RGB, OCTET* R, OCTET *G, OCTET* B, int nW, int nH)
{
    planR(R, RGB, nW * nH);
    planV(G, RGB, nW * nH);
    planB(B, RGB, nW * nH);
}
void imageFromRGB(OCTET* R, OCTET* G, OCTET* B, OCTET* output, int width, int height)
{
    for (int i = 0; i < width * height; i++)
    {
        output[3*i + 0] = R[i];
        output[3*i + 1] = G[i];
        output[3*i + 2] = B[i];
    }
}
// On peut traiter les images comme celle de l'algo flou2 car c'est des images au niveaux de gris 
// 

void flou(OCTET* ImgIn, OCTET* ImgOut, int nW, int nH)
{
  for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
       ImgOut[i*nW+j]= (ImgIn[i*nW+j]+ImgIn[(i-1)*nW+j]+ImgIn[(i+1)*nW+j]+ImgIn[i*nW+(j-1)]+ImgIn[i*nW+(j+1)]+ImgIn[(i-1)*nW+(j-1)]+ImgIn[(i-1)*nW+(j+1)]+ImgIn[(i+1)*nW+(j-1)]+ImgIn[(i+1)*nW+(j+1)])/9;

     }
} 

int main(int argc, char* argv[])
{

  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   
    // On déclare toutes les images dont on aura besoin :
    // -> L'image initiale, les canaux R/G/B ainsi que leur transformation, puis l'image finale
    OCTET *ImgIn, *Rouge, *Vert, *Bleu, *R_flou, *V_flou, *B_flou, *ImgOut;
    // Traitement habituel : calculer les dimensions de l'image
   
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nW * nH;
    int nTaille3 = 3 * nTaille;
    // On alloue l'espace pour TOUTES les images à utiliser. Seules ImgIn et ImgOut ont une taille nTaille3
    // car elles sont en couleur. Les canaux R/G/B sont en niveau de gris.
    allocation_tableau(ImgIn, OCTET, nTaille3);
    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(Rouge, OCTET, nTaille);
    allocation_tableau(Vert, OCTET, nTaille);
    allocation_tableau(Bleu, OCTET, nTaille);
    allocation_tableau(R_flou, OCTET, nTaille);
    allocation_tableau(V_flou, OCTET, nTaille);
    allocation_tableau(B_flou, OCTET, nTaille);
    lire_image_ppm(cNomImgLue, ImgIn, nTaille);

    // J'ai créé une fonction pour récuperer les R/G/B, mais c'est simplement l'utilisation de la fonction
    // "planR"/"planV"/"planB" définie par votre prof dans image_ppm.h.
    separateRGB(ImgIn, Rouge, Vert, Bleu, nW, nH);

    // On applique les transformations à chaque canal individuellement
    // (Ici une dilatation, mais ça peut être le flou, le seuillage, le calcul de contours, etc...)
    flou(Rouge, R_flou, nW, nH);
    flou(Vert, V_flou, nW, nH);
    flou(Bleu, B_flou, nW, nH);

    // On mixe les 3 canaux R/G/B en une seule image "ImgOut"
    imageFromRGB(R_flou, V_flou, B_flou, ImgOut, nW, nH);
    // On enregistre, fini!
    ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);

    // Désallocation mémoire
    free(ImgIn);
    free(ImgOut);
    free(Rouge);
    free(Vert);
    free(Bleu);
    free(R_flou);
    free(V_flou);
    free(B_flou);
    return 0;
} 