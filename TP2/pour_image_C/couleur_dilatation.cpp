#include "image_ppm.h"
#include <stdio.h>
void separateRGB(OCTET* RGB, OCTET* R, OCTET *G, OCTET* B, int nW, int nH)
{
    planR(R, RGB, nW * nH);
    planV(G, RGB, nW * nH);
    planB(B, RGB, nW * nH);
}
void imageFromRGB(OCTET* R, OCTET* G, OCTET* B, OCTET* output, int width, int height)
{
    for (int i = 0; i < width * height; i++)
    {
        output[3*i + 0] = R[i];
        output[3*i + 1] = G[i];
        output[3*i + 2] = B[i];
    }
}
// TP2 - Exercice 2 : Dilatation
// La facon de faire est très identique à l'erosion, sauf que :
// - Methode 1 : on inclut le pixel courant dans le foreground si au moins un voisin est à 255
// - Methode 2 : on affecte la valeur maximale des voisins au pixel courant
void dilatation(OCTET* input, OCTET* output, int width, int height)
{
    // On va modifier chaque pixel de chaque ligne et chaque colonne
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            // Methode 1 : On considere le pixel courant hors du foreground
            bool pixel_foreground = false;
            // Methode 2 : On considere la valeur maximale des pixels à 0
            OCTET maxPixel = 0;

            for (int _i = -1; _i <= 1; _i ++) {
                for (int _j = -1; _j <= 1; _j ++) {
                        // Check des boundaries
                    if ((i + _i) < 0 || (i + _i) >= height || (j + _j) < 0 || (j + _j) >= width)
                        continue;
                    // Methode 1 : Si au moins un voisin est dans le foreground, on considère le pixel courant dedans
                    pixel_foreground |= input[(i + _i) * width + (j + _j)] > 127;
                    // Methode 2 : On affecte la valeur maximale des pixels voisins à notre pixel courant
                    maxPixel = std::fmax(maxPixel, input[(i + _i) * width + (j + _j)]);
                }
            }
        // Methode 1 : On affecte le pixel a 0 ou 255
            if (pixel_foreground) {
                output[i * width + j] = 255;
            } else {
                output[i * width + j] = 0;
            }
            // Methode 2 : on donne la valeur minimale au pixel
            output[i * width + j] = maxPixel;
        }
    } 
}
int main(int argc, char* argv[])
{

   char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   
    // On déclare toutes les images dont on aura besoin :
    // -> L'image initiale, les canaux R/G/B ainsi que leur transformation, puis l'image finale
    OCTET *ImgIn, *Rouge, *Vert, *Bleu, *R_dilatee, *V_dilatee, *B_dilatee, *ImgOut;
    // Traitement habituel : calculer les dimensions de l'image
    int nW, nH;
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    int nTaille = nW * nH;
    int nTaille3 = 3 * nTaille;
    // On alloue l'espace pour TOUTES les images à utiliser. Seules ImgIn et ImgOut ont une taille nTaille3
    // car elles sont en couleur. Les canaux R/G/B sont en niveau de gris.
    allocation_tableau(ImgIn, OCTET, nTaille3);
    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(Rouge, OCTET, nTaille);
    allocation_tableau(Vert, OCTET, nTaille);
    allocation_tableau(Bleu, OCTET, nTaille);
    allocation_tableau(R_dilatee, OCTET, nTaille);
    allocation_tableau(V_dilatee, OCTET, nTaille);
    allocation_tableau(B_dilatee, OCTET, nTaille);
    lire_image_ppm(cNomImgLue, ImgIn, nTaille);

    // J'ai créé une fonction pour récuperer les R/G/B, mais c'est simplement l'utilisation de la fonction
    // "planR"/"planV"/"planB" définie par votre prof dans image_ppm.h.
    separateRGB(ImgIn, Rouge, Vert, Bleu, nW, nH);

    // On applique les transformations à chaque canal individuellement
    // (Ici une dilatation, mais ça peut être le flou, le seuillage, le calcul de contours, etc...)
    dilatation(Rouge, R_dilatee, nW, nH);
    dilatation(Vert, V_dilatee, nW, nH);
    dilatation(Bleu, B_dilatee, nW, nH);

    // On mixe les 3 canaux R/G/B en une seule image "ImgOut"
    imageFromRGB(R_dilatee, V_dilatee, B_dilatee, ImgOut, nW, nH);
    // On enregistre, fini!
    ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);

    // Désallocation mémoire
    free(ImgIn);
    free(ImgOut);
    free(Rouge);
    free(Vert);
    free(Bleu);
    free(R_dilatee);
    free(V_dilatee);
    free(B_dilatee);
    return 0;
} 
