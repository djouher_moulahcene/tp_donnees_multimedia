// test_couleur.cpp : Seuille une image en niveau de gris
// on seuils apres avoir seuiller car on va travailler qu'avec des images binaires 
#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[3],"%d",&S);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
    /*  =====> permettre de boucher des petits trous isolés dans les objets de l’image.
    /*  =====> c'est l'inverse de l'érosion :
       Si le pixel courant est blanc et que tous ces voisins les 8 sont à blanc alors il reste blanc .
       Sinon si au moins l'un de ces voisisn n'est pas blanc (c'est à dire noir) alors le pixel courant = 0 (noir )
       
    */
//

 // On va modifier chaque pixel de chaque ligne et chaque colonne
    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            // Methode 1 : On considere le pixel courant hors du foreground
            bool pixel_foreground = false;
            // Methode 2 : On considere la valeur maximale des pixels à 0
            OCTET maxPixel = 0;

            for (int _i = -1; _i <= 1; _i ++) {
                for (int _j = -1; _j <= 1; _j ++) {
                        // Check des boundaries
                    if ((i + _i) < 0 || (i + _i) >= nH || (j + _j) < 0 || (j + _j) >= nW)
                        continue;
                    // Methode 1 : Si au moins un voisin est dans le foreground, on considère le pixel courant dedans
                    pixel_foreground |= ImgIn[(i + _i) * nW + (j + _j)] > 127;
                    // Methode 2 : On affecte la valeur maximale des pixels voisins à notre pixel courant
                    maxPixel = std::fmax(maxPixel, ImgIn[(i + _i) * nW + (j + _j)]);
                }
            }
            // Methode 1 : On affecte le pixel a 0 ou 255
            if (pixel_foreground) {
                ImgOut[i * nW + j] = 255;
            } else {
                ImgOut[i * nW + j] = 0;
            }
            // Methode 2 : on donne la valeur minimale au pixel
            ImgOut[i * nW + j] = maxPixel;
        }
    }
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
 }

