// test_couleur.cpp : Seuille une image en niveau de gris
// on seuils apres avoir seuiller car on va travailler qu'avec des images binaires 
#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[3],"%d",&S);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
    /*  =====> permettre de boucher des petits trous isolés dans les objets de l’image.
    /*  =====> c'est l'inverse de l'érosion :
       Si le pixel courant est blanc et que tous ces voisins les 8 sont à blanc alors il reste blanc .
       Sinon si au moins l'un de ces voisisn n'est pas blanc (c'est à dire noir) alors le pixel courant = 0 (noir )
       
    */
//

 for (int i=1; i < nH-1; i++){ 
   for (int j=1; j < nW-1; j++)
     {
      
      
    if (ImgIn[i*nW+j]==255){
        if (ImgIn[(i-1)*nW+(j-1)]==255){
          ImgOut[i*nW+j]=255;
        }
        else if (ImgIn[(i-1)*nW+j]==255){
          ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[(i-1)*nW+(j+1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[i*nW+(j-1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[i*nW+(j+1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[(i+1)*nW+(j-1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[(i+1)*nW+j]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[i*nW+(j+1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else {ImgOut[i*nW+j]=0;}
     }
    else {ImgOut[i*nW+j]=0;}
  }
    }
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
 }

