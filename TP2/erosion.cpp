// test_couleur.cpp : Seuille une image en niveau de gris
// on seuils apres avoir seuiller car on va travailler qu'avec des images binaires 
#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[3],"%d",&S);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   /* L'erosion nous permet de supprimer les points isolés quand on va faire la transformation morphologique qui modfie la valeur du pixel
   courant en le fait selon ces 8 voisins :
   Si le pixel courant est noir et que en parcourant ces voisins ces derniers sont noir alors le pixel central (courant ) il reste noir .
   Sinon si l'un au moins l'un de ces voisins est blanc alors alors le met à blanc et donc 255 pixel .
   sinon il reste blanc */

 for (int i=1; i < nH-1; i++){ 
   for (int j=1; j < nW-1; j++)
     {
      
       
    if (ImgIn[i*nW+j]==0){
        if (ImgIn[(i-1)*nW+(j-1)]==0){
          ImgOut[i*nW+j]=0;
        }
        else if (ImgIn[(i-1)*nW+j]==0){
          ImgOut[i*nW+j]=0;
      }
      else if (ImgIn[(i-1)*nW+(j+1)]==0){
        ImgOut[i*nW+j]=0;
      }
      else if (ImgIn[i*nW+(j-1)]==0){
        ImgOut[i*nW+j]=0;
      }
      else if (ImgIn[i*nW+(j+1)]==0){
        ImgOut[i*nW+j]=0;
      }
      else if (ImgIn[(i+1)*nW+(j-1)]==0){
        ImgOut[i*nW+j]=0;
      }
      else if (ImgIn[(i+1)*nW+j]==0){
        ImgOut[i*nW+j]=0;
      }
      else if (ImgIn[i*nW+(j+1)]==0){
        ImgOut[i*nW+j]=0;
      }
      else {ImgOut[i*nW+j]=255;}
     }
    else {ImgOut[i*nW+j]=255;}
  }
    }
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
 }
