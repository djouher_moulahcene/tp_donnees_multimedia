#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB, k;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm k \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&k);
  

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
  
    allocation_tableau(ImgIn, OCTET, nTaille); // octet c'est un type char non signé
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for (int i=0; i < nW; i++)
     {
       for (int j=0; j < nH; j++){
            if (ImgIn[i*nW+j]+k > 255){
                ImgOut[i*nW+j]=255;
            }
            else if (ImgIn[i*nW+j]+k < 0){
                ImgOut[i*nW+j]=0;
            }
            else ImgOut[i*nW+j]= ImgIn[i*nW+j]+k;
       }
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   return 1;
}
