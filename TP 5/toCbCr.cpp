// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite1[250], cNomImgEcrite2[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.ppm Cb.pgm Cr.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite1);
   sscanf (argv[3],"%s",cNomImgEcrite2);
  

   OCTET *ImgIn, *ImgOut1, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, nTaille3); // octet c'est un type char non signé
   lire_image_ppm(cNomImgLue, ImgIn, nTaille);
   allocation_tableau(ImgOut1, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);
	
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       // on met diviser par 3 car c'est des images pgm et la boucle va jusqu'à ntaille3 avec Cb = ntaille 
        // Cb 
       ImgOut1[i/3]= ((-0.1687)*nR) - (0.3313*nG) + (0.5*nB) +128;
       ImgOut1[(i+1)/3]= ((-0.1687)*nR) - (0.3313*nG) + (0.5*nB) +128;
       ImgOut1[(i+2)/3]=((-0.1687)*nR) - (0.3313*nG) + (0.5*nB) +128;

    // Cr

        ImgOut2[i/3]= (0.5*nR)- (0.4187*nG) - (0.0813*nB) + 128;
        ImgOut2[(i+1)/3]= (0.5*nR) - (0.4187*nG) - (0.0813*nB) + 128;
        ImgOut2[(i+2)/3]=(0.5*nR) - (0.4187*nG) - (0.0813*nB) + 128;

     }

     

   ecrire_image_pgm(cNomImgEcrite1, ImgOut1,  nH, nW);
    ecrire_image_pgm(cNomImgEcrite2, ImgOut2,  nH, nW);

   free(ImgIn); free(ImgOut1); free(ImgOut2);
   return 1;
}
