// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLueY[250], cNomImgLueCr[250] , cNomImgLueCb[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageInY.pgm ImageInCr.pgm ImageInCb.pgm ImageOut.ppm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLueY) ;
   sscanf (argv[2],"%s",cNomImgLueCr) ;
   sscanf (argv[3],"%s",cNomImgLueCb) ;
   sscanf (argv[4],"%s",cNomImgEcrite);
  

   OCTET *ImgInY, *ImgInCr,*ImgInCb, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueCr, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueCb, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;

    allocation_tableau(ImgInY, OCTET, nTaille3); // octet c'est un type char non signé
   lire_image_pgm(cNomImgLueY, ImgInY, nTaille);

   allocation_tableau(ImgInCr, OCTET, nTaille3); // octet c'est un type char non signé
   lire_image_pgm(cNomImgLueCr, ImgInCr, nTaille);

   allocation_tableau(ImgInCb, OCTET, nTaille3); // octet c'est un type char non signé
   lire_image_pgm(cNomImgLueCb, ImgInCb, nTaille);


   allocation_tableau(ImgOut, OCTET, nTaille3);
	
   
         for (int j=0; j < nTaille3 ; j+=3){ 
             
            int  Y=ImgInY[j/3];
             int Cr=ImgInCr[j/3];
            int  Cb=ImgInCb[j/3]; 

            if ((Y!=0) && (Y!=255) && (Cr!=0) && (Cr!=255) && (Cb!=0) && (Cb!=255)){  

            ImgOut[j]= Y + (1.402*(Cr - 128));
            ImgOut[j+1]=  Y +(1.772*(Cb - 128));
            ImgOut[j+2]= Y - (0.34414*(Cb - 128)) - (0.714414*(Cr - 128));
     }
     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgInY); free(ImgInCr);free(ImgInCb);free(ImgOut);
   return 1;
}
