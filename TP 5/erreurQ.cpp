// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char ImageYIn[250], nomImg1[250] ;
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn1.pgm ImageIn2.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",ImageYIn);
   sscanf (argv[2],"%s",nomImg1);

   OCTET *ImgIn, *ImgInY;
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(nomImg1, ImgIn, nH * nW);
   allocation_tableau(ImgInY, OCTET, nTaille);
   lire_image_pgm(ImageYIn, ImgInY, nH * nW);

   int err=0;
    for (int i=0;i < nW;i++){
      for (int j=0;j < nH ; j++){
         err += pow((ImgIn[i*nW+j] - ImgInY[i*nW+j]),2);

      }
    }
     
     err=err/(nW*nH);

     printf("l'erreur obtenu est %d\n",err);
   
   free(ImgIn);free(ImgInY);
  
   return 1;
}
