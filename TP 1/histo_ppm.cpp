#include <stdio.h>
#include "image_ppm.h"
#include "fstream"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int TabR[256],TabV[256],TabB[256];
  std::ofstream fichier3("histo_ppm.dat");

    if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue);

     OCTET *ImgIn;

        
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, 3*nTaille);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   for(int k = 0;k < 256;k++)
   {
       TabR[k] = 0;
       TabV[k] = 0;
       TabB[k] = 0;
   }

  int i=0; 
  while (i < 3*nH){
    for (int j=0; j < nW; j++)
      {
       TabR[ImgIn[i*nW+j]]++;
       TabV[ImgIn[i*nW+j+1]]++;
       TabB[ImgIn[i*nW+j+2]]++;
      }
      i = i +3;
      }

   for(int i = 0;i < 256;i++)
   {
    fichier3 << i << "  " << TabR[i] << "  " << TabV[i] << "  " << TabB[i] << "\n";
   }

   return 1;
}