#include <stdio.h>
#include <fstream>
#include "image_ppm.h"
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
  
   OCTET *ImgIn; // c'est des pointeur
   
   int tab[256];
   std::ofstream fichier("histo.dat");
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   
   
    for (int i=0;i<= 256;i++){
    tab[i]=0;
      }

    for (int i=0; i < nH; i++){
   for (int j=0; j < nW; j++)
     {
       tab[ImgIn[i*nW+j]]++;
     }
     }
// gnuplot c'est un éditeur de courbe 

    for (int i=0;i<=256;i++){
    fichier << i << "  " << tab[i] << "\n";
    }
   

   return 1;
 
}
