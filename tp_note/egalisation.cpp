#include <stdio.h>
#include <fstream>
#include "image_ppm.h"
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm imageout \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[1],"%s",cNomImgEcrite) ;
  
   OCTET *ImgIn, *ImgOut; // c'est des pointeur
   
  
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   

     for (int i=0; i < nH; i++){
   for (int j=0; j < nW; j++)
     {
      
     }
     }
   
   ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);

    // Désallocation mémoire
    free(ImgIn);
    free(ImgOut);
    
   return 1;
 
}
