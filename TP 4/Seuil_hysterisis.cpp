#include <stdio.h>
#include "image_ppm.h"
#include <bits/stdc++.h>

/* on calcul le gradient vertical de chaque point de l'image tel qu'on multiplie le pixel courant par -1 et son voisin droite par 1  */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SB,SH;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  Seuil1 Seuil2\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[3],"%d",&SH);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i=1; i < nH-1; i++){ 
   for (int j=1; j < nW-1; j++)
     {
      
      if (ImgIn[i*nW+j] <= SB) ImgOut[i*nW+j]=0;
      else if ( ImgIn[i*nW+j] >= SH) ImgOut[i*nW+j]=255;
     

    if((SB < ImgIn[i*nW+j]) and (ImgIn[i*nW+j] < SH)){
         if (ImgIn[(i-1)*nW+(j-1)]==255){
          ImgOut[i*nW+j]=255;
        }
        else if (ImgIn[(i-1)*nW+j]==255){
          ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[(i-1)*nW+(j+1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[i*nW+(j-1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[i*nW+(j+1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[(i+1)*nW+(j-1)]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[(i+1)*nW+j]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgIn[i*nW+(j+1)]==255){
        ImgOut[i*nW+j]=255;
      }
    else {ImgOut[i*nW+j]=0;}

     }
    }
   }
ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}