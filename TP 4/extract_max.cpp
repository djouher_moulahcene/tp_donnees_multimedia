#include <stdio.h>
#include "image_ppm.h"
#include <bits/stdc++.h>

/* on calcul le gradient vertical de chaque point de l'image tel qu'on multiplie le pixel courant par -1 et son voisin droite par 1  */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  Seuil\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&S);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i=1; i < nH-1; i++){ 
   for (int j=1; j < nW-1; j++)
     {
      
      if (ImgIn[i*nW+j] < S) ImgOut[i*nW+j]=0;
      else ImgOut[i*nW+j]=255;
     }
    }

ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}