// profil d'une ligne c'est voir les variations du niveau de gris d'une image 
// en entrée on aura une image et numero de ligne qu'on veut visualiser (c ou l) 
#include <stdio.h>
#include <fstream>
#include "image_ppm.h"


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, indice,num;
  
  if (argc != 4) 
     {
        printf("Usage: ImageIn.pgm ligne/colonne(indice) num \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%d",&indice);
   sscanf (argv[3],"%d",&num);

   OCTET *ImgIn; 

   int tab[256];
   std::ofstream fichier2("profil.dat");
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nTaille);
   


// initialiser le tableau à 0
    for (int i=0;i<= 256;i++){
    tab[i]=0;
      }
//  indice==0 alors on fixe la ligne et on parcour les colonnes 
      if (indice==0){
       if(num < nH){ 
      	for (int j=0;j<nW;j++){
      		tab[ImgIn[num*nW+j]]++;
      	}
       }
      }
      else if (indice==1) {
        if(num < nW){  
      	 for (int i=0;i<nH-1;i++){
      		tab[ImgIn[i*nW+num]]++;
      	 }
        }
      }
      else printf("erreur");

      // gnuplot c'est un éditeur de courbe 
     
    for (int i=0;i<=256;i++){
    fichier2 << i << "  " << tab[i] << "\n";
    }

    fichier2.close();
   
  
   free(ImgIn); 

   return 1;
}
 


	