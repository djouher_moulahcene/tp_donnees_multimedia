#include "image_ppm.h"



int main(int argc, char* argv[])
{

  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   
    // On déclare toutes les images dont on aura besoin :
    // -> L'image initiale, les canaux R/G/B ainsi que leur transformation, puis l'image finale
    OCTET *ImgIn, *Rouge, *Vert, *Bleu, *ImgOut;
    // Traitement habituel : calculer les dimensions de l'image
   
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nW * nH;
    int nTaille3 = 3 * nTaille;
    
    // On alloue l'espace pour TOUTES les images à utiliser. Seules ImgIn et ImgOut ont une taille nTaille3
    // car elles sont en couleur. Les canaux R/G/B sont en niveau de gris.
    allocation_tableau(ImgIn, OCTET, nTaille3);
    allocation_tableau(ImgOut, OCTET, nTaille3);

    allocation_tableau(Rouge, OCTET, nTaille);
    allocation_tableau(Vert, OCTET, nTaille);
    allocation_tableau(Bleu, OCTET, nTaille);

    lire_image_ppm(cNomImgLue, ImgIn, nTaille);

    planR(Rouge, ImgIn, nW * nH);
    planV(Vert, ImgIn, nW * nH);
    planB(Bleu,ImgIn, nW * nH);
    
    for (int i=0;i<nTaille3;i+=3){
      ImgOut[i]=Rouge[i/3]; // car ce sont des images au niveau de gris
      ImgOut[i+1]=Vert[i/3];
      ImgOut[i+2]=Bleu[i/3];
    }
    
    ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);

    // Désallocation mémoire
    free(ImgIn);
    free(ImgOut);
    free(Rouge);
    free(Vert);
    free(Bleu);
    
    return 0;
} 